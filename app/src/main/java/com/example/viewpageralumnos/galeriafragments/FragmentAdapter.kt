package com.example.viewpageralumnos.galeriafragments

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class FragmentAdapter(
    val listadoFragment : List<Fragment>,
    fragment: Fragment
) : FragmentStateAdapter(
    fragment
){
    override fun getItemCount(): Int {
        return listadoFragment.size
    }

    override fun createFragment(position: Int): Fragment {
        val fragment = listadoFragment[position]
        return fragment
    }
}