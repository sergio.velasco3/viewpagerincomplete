package com.example.viewpageralumnos.galeriaavanzada

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpageralumnos.databinding.CeldaParaAvanzadaBinding


/**
 * Created by sergi on 21/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class AdapterAvanzado(val listaPeliculas: List<Pelicula>) :
    RecyclerView.Adapter<AdapterAvanzado.EstaCelda>() {

    inner class EstaCelda(val binding: CeldaParaAvanzadaBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EstaCelda {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: EstaCelda, position: Int) {
        TODO("Not yet implemented")
    }


}