package com.example.viewpageralumnos

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.viewpageralumnos.databinding.FragmentMainBinding
import com.example.viewpageralumnos.tutorial.TutorialActivity

class FragmentSelector : Fragment() {

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.botonGaleria.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSelector_to_galeriaFragment)
        }
        binding.botonLayout.setOnClickListener {
        }
        binding.botonFragments.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentSelector_to_fragmentsInFragment)
        }
        binding.botonTutorial.setOnClickListener {
            startActivity(
                Intent(
                    requireContext(),
                    TutorialActivity::class.java
                )
            )
        }
    }

}