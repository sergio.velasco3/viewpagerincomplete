package com.example.viewpageralumnos.galeriasimple

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpageralumnos.R
import com.example.viewpageralumnos.databinding.FragmentGaleriaSimpleBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class GaleriaFragment : Fragment() {

    private lateinit var binding: FragmentGaleriaSimpleBinding
    private var primeraCarga = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentGaleriaSimpleBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listaPeliculas =
            listOf(R.drawable.img_1, R.drawable.img_2, R.drawable.img_3, R.drawable.img_4)


    }
}