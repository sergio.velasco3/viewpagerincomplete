package com.example.viewpageralumnos.tutorial

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpageralumnos.tutorial.fragments.Tutorial21
import com.example.viewpageralumnos.tutorial.fragments.Tutorial22
import com.example.viewpageralumnos.tutorial.fragments.Tutorial33


/**
 * Created by sergi on 22/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class TutorialAdapter2(
    val activity: FragmentActivity
    ) :
    FragmentStateAdapter(activity) {

    override fun getItemCount(): Int {
        return TutorialActivity.NUM_FRAGMENTS
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            1 -> Tutorial22()
            2 -> Tutorial33()
            else -> Tutorial21()
        }
    }
}