package com.example.viewpageralumnos.tutorial.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.viewpageralumnos.databinding.Tutorial1Binding
import com.example.viewpageralumnos.databinding.Tutorial21Binding
import com.example.viewpageralumnos.tutorial.TutorialActivity


/**
 * Created by sergi on 22/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class Tutorial21: Fragment() {

    private lateinit var binding: Tutorial21Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = Tutorial21Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btForward.setOnClickListener {
            (requireActivity() as TutorialActivity).irAdelante()
        }
    }
}