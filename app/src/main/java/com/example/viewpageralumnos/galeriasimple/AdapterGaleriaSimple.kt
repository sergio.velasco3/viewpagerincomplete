package com.example.viewpageralumnos.galeriasimple

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.viewpageralumnos.databinding.CeldaParaGaleriaBinding


/**
 * Created by sergi on 21/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class AdapterGaleriaSimple(val listaImagenes: List<Int>) :
    RecyclerView.Adapter<AdapterGaleriaSimple.MiCelda>() {

    inner class MiCelda(val binding: CeldaParaGaleriaBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiCelda {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: MiCelda, position: Int) {
        TODO("Not yet implemented")
    }


}