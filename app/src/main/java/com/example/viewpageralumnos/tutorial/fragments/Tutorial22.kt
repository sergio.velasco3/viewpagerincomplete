package com.example.viewpageralumnos.tutorial.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.viewpageralumnos.databinding.Tutorial22Binding
import com.example.viewpageralumnos.tutorial.TutorialActivity


/**
 * Created by sergi on 22/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class Tutorial22 : Fragment() {

    private lateinit var binding: Tutorial22Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = Tutorial22Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val preferences = requireActivity().getSharedPreferences("prefs", Context.MODE_PRIVATE)

        val espania = preferences.getBoolean("espania", false)
        binding.switch1.isChecked = espania

        val boletin = preferences.getBoolean("boletin", false)
        binding.switch2.isChecked = boletin

        binding.btForward.setOnClickListener {
            val myActivity = requireActivity() as TutorialActivity
            myActivity.saveValores(binding.switch1.isChecked, binding.switch2.isChecked)
            myActivity.irAdelante()
        }

        binding.btBack.setOnClickListener {

            (requireActivity() as TutorialActivity).mostrarDIalog("No puedes ir hacia atrás", "Atención")
        }
    }
}