package com.example.viewpageralumnos.tutorial

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpageralumnos.databinding.TutorialActivityBinding


/**
 * Created by sergi on 22/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class TutorialActivity : AppCompatActivity() {

    companion object {
        const val NUM_FRAGMENTS = 3
        const val PI = 3.1415
    }
    val aficionado = "aficionado"

    private lateinit var binding: TutorialActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TutorialActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val adapter = TutorialAdapter2(this)
        binding.viewpager.adapter = adapter

        binding.btFinish.setOnClickListener {
            finish()
            // o abrir otra pantalla
        }

        binding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                title = "Paso ${position + 1}"

                if (position == NUM_FRAGMENTS - 1)
                    binding.btFinish.visibility = View.VISIBLE
                else binding.btFinish.visibility = View.INVISIBLE
            }
        })
    }

    fun irAtras() {
        val position = binding.viewpager.currentItem
        if (position > 0)
            binding.viewpager.currentItem = position - 1
    }

    fun irAdelante() {
        val position = binding.viewpager.currentItem
        if (position < NUM_FRAGMENTS - 1)
            binding.viewpager.currentItem = position + 1
    }

    fun saveValores(estaEspania: Boolean, recibirBoletin: Boolean) {
        val pref = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean("espania", estaEspania)
        editor.putBoolean("boletin", recibirBoletin)
        editor.apply()
    }

    fun mostrarDIalog(mensaje: String, titulo: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(titulo)
        builder.setMessage(mensaje)
        builder.setPositiveButton("cerrar", null)
        builder.create().show()
    }
}