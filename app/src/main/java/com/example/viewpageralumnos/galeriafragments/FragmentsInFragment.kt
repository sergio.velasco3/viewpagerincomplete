package com.example.viewpageralumnos.galeriafragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.example.viewpageralumnos.galeriafragments.fragments.Tutorial1
import com.example.viewpageralumnos.galeriafragments.fragments.Tutorial2
import com.example.viewpageralumnos.galeriafragments.fragments.Tutorial3
import com.example.viewpageralumnos.databinding.FragmentWithFragmentsBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class FragmentsInFragment : Fragment() {

    private lateinit var binding: FragmentWithFragmentsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentWithFragmentsBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listaFragment = listOf(Tutorial1(), Tutorial2(), Tutorial3())

        val adaptador = FragmentAdapter(listaFragment, this)
        binding.viewpager.adapter = adaptador

        binding.btBack.setOnClickListener {
            val variable = binding.viewpager.currentItem
            if (variable >= 1)
                binding.viewpager.currentItem = variable - 1
        }

        binding.btFinish.setOnClickListener {
//            requireActivity().finish()
            findNavController().navigateUp()
        }

        binding.btForward.setOnClickListener {
            val variable = binding.viewpager.currentItem
            if (variable < listaFragment.size - 1)
                binding.viewpager.currentItem = variable + 1
        }

        binding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                with(binding) {
                    if (position == 0) {
                        btBack.isVisible = false
                        btFinish.isVisible = false
                        btForward.isVisible = true
                    } else if (position == 1) {
                        btBack.isVisible = true
                        btFinish.isVisible = false
                        btForward.isVisible = true
                    } else {
                        btBack.isVisible = true
                        btFinish.isVisible = true
                        btForward.isVisible = false
                    }
                }
            }
        })
    }
}